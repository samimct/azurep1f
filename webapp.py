#!/usr/bin/python
from flask import Flask
import requests
import json
import socket
import sys
app = Flask(__name__)


@app.route('/')
def hello_world():
    h = socket.gethostname()
    s =  '<h1>Welcome to {0}</h1>'.format(h)
    u = 'http://{0}:5000/cvm/api/v1.0/t'.format(sys.argv[1])
    print(u)
    res = requests.get(u)
    j = res.text
    print res.text
    data = json.loads(j)
    id = data['t'][0]['id']
    vm = data['t'][0]['vm']
    dr = data['t'][0]['description']
    s += "id: {0}<br>vm: {1}<br>{2}".format(id, vm, dr)
    return s

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
